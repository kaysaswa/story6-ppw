
$(document).ready(function() {
    $('.accordion-toggle').on('click', function(event) {
      event.preventDefault();
      // create accordion variables
      var accordion = $(this);
      var accordionContent = accordion.next('.accordion-content');
  
      // toggle accordion link open class
      accordion.toggleClass("open");
      // toggle accordion content
      accordionContent.slideToggle(250);
  
    });
  });

  $(document).ready(function () {
    $('.move-down').click(function (e) {
        var self = $(this),
            item = self.parents('div.accordion-container'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    $('.move-up').click(function (e) {
        var self = $(this),
            item = self.parents('div.accordion-container'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
});

$(".search-txt").keyup(function(){
    var word = $(".search-txt").val();
    //console.log(word);
    // panggil dengan ajax
    $.ajax({
        url:'data?q=' + word,
        success:function(data){
            //console.log(data);
            var array_items = data.items;
            //console.log(array_items);
            $(".flex-container").empty();
            for(i=0;i<array_items.length;i++){
            var judul = array_items[i].volumeInfo.title;
            var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
            $(".flex-container").append("<div>" +"<h4>"+judul+"</h4>" +"<br><img src="+gambar+"></div>");
            }
        }
    })
})

var modal=document.getElementById('loginpage');

window.onclick=function(event){
    if(event.target==modal){
        modal.style.display="none";

    }
}
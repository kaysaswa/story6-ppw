from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def home(request):
    return render(request,"story9/hi.html")

def loginFunc(request):
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request,user)
            return redirect('story9:home')
    else:
        form=AuthenticationForm()
    return render(request,"story9/login.html",{'form':form})

def logoutFunc(request):
    logout(request)
    return redirect('/')

def signup(request):
    if request.method == "POST":
        form=UserCreationForm(data=request.POST)
        if form.is_valid():
            user=form.save()
            login(reques.user)
            return redirect('story9:home')
    else:
        form=UserCreationForm()
    return render(request,"story9/signup.html",{'form':form})
from django.contrib import admin
from django.urls import path,include
from .views import home,loginFunc,logoutFunc,signup

app_name = 'story9'

urlpatterns=[
    path('home',home,name='home'),
    path('login',loginFunc,name='login'),
    path('logout',logoutFunc,name='logout'),
    path('signup',signup,name='signup')
]
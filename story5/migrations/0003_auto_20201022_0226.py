# Generated by Django 3.1.2 on 2020-10-21 19:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5', '0002_auto_20201017_0450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postcourse',
            name='Credit',
            field=models.CharField(choices=[('one', '1'), ('two', '2'), ('three', '3'), ('four', '4'), ('five', '5'), ('six', '6'), ('seven', '7')], default='1', max_length=20),
        ),
        migrations.AlterField(
            model_name='postcourse',
            name='Semester',
            field=models.CharField(choices=[('Gasal', 'Gasal 2020/2021')], default='Gasal 2020/2021', max_length=20),
        ),
    ]

from django.test import TestCase,Client
from django.urls import resolve
from .views import home

# Create your tests here.

#test url ada atau tidak
class Test (TestCase):
    def test_url_Events_untuk_ListEvents(self):
        response = Client().get('/story7/')
        self.assertEquals(response.status_code,200)

    def test_apakah_ada_events_dan_tombol_join_us(self):
        response = Client().get('/story7/')
        template_name = response.content.decode('utf8')
        self.assertIn("My Name",template_name)
        self.assertIn("KAYSA SWA",template_name)
        self.assertIn("My Hobbies",template_name)
        self.assertIn("Do I like K-Pop?",template_name)   
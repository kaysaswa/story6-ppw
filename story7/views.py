from django.shortcuts import render

# Create your views here.
def home(request):
    page = 'STORY 7'
    response = {'title' : page}
    return render(request,'story7/home.html',response)

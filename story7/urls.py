from django.urls import path

from .views import home

app_name = 'story7'

urlpatterns = [
    path('', home, name='home'),
]
from django.urls import path

from .views import home,fungsi_data

app_name = 'story8'

urlpatterns = [
    path('', home, name='home'),
    path('data/',fungsi_data,name='fungsi_data')
]
from django.test import TestCase,Client
from django.urls import resolve
from .views import home

# Create your tests here.

#test url ada atau tidak
class Test (TestCase):
    def test_url_Events_untuk_ListEvents(self):
        response = Client().get('/story8/')
        self.assertEquals(response.status_code,200)

    def test_apakah_ada_events_dan_tombol_join_us(self):
        response = Client().get('/story8/')
        template_name = response.content.decode('utf8')
        self.assertIn("KAYSA SWA",template_name)
        self.assertIn("Book Search Engine",template_name)
        self.assertIn("FIND ME HERE",template_name)
        self.assertIn("KAYSASWA",template_name)


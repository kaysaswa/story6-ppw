from django.test import TestCase,Client
from django.urls import resolve
from .views import Events,AddEvent,AddMember
from .models import FormEvent,FormMember
from .forms import EventForm,MemberForm


# Create your tests here.

#test url ada atau tidak
class Test (TestCase):
    def test_url_Events_untuk_ListEvents(self):
        response = Client().get('/Events/')
        self.assertEquals(response.status_code,200)    

#test ada kata-kata events dan join us 
    def test_apakah_ada_events_dan_tombol_join_us(self):
        response = Client().get('/Events/')
        template_name = response.content.decode('utf8')
        self.assertIn("EVENTS",template_name)
        self.assertIn("KAYSA SWA",template_name)
        self.assertIn("EVENTS",template_name)
        self.assertIn("ADD EVENT",template_name)

    def test_Events_ada_html(self): 
        response = Client().get('/Events/')
        self.assertTemplateUsed(response,'Events/Events.html')


    def test_AddEvents_ada_html(self): 
        response = Client().get('/Events/AddEvent')
        self.assertTemplateUsed(response,'Events/AddEvent.html')
    
    def test_AddMember_ada_html(self): 
        response = Client().get('/Events/AddMember/<event_id>')
        self.assertTemplateUsed(response,'Events/AddMember.html')
    
    def test_model_can_create_new_event(self):
        # Creating a new activity
        new_activity = FormEvent.objects.create(Event_Name='PPW')

        # Retrieving all available activity
        counting_all_available_event = FormEvent.objects.all().count()
        self.assertEqual(counting_all_available_event, 1)
    
    def test_model_can_create_new_member(self):
        # Creating a new activity
        new_activity = FormMember.objects.create(First_Name='Kaysa',Last_Name='Syifa')

        # Retrieving all available activity
        counting_all_available_member = FormMember.objects.all().count()
        self.assertEqual(counting_all_available_member, 1)

    def test_form_event_input_has_placeholder_and_css_classes(self):
        form = EventForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('placeholder="Enter Event Name"', form.as_p())
        self.assertIn('style="#FF0C55"', form.as_p())
        self.assertIn('id="Event_Name"', form.as_p())
        self.assertIn('name="Event_Name"', form.as_p())
    
    def test_form_member_input_has_placeholder_and_css_classes(self):
        form = MemberForm()
        self.assertIn('class="form-control"', form.as_p())
        self.assertIn('placeholder="Enter Your First Name"', form.as_p())
        self.assertIn('placeholder="Enter Your Last Name"', form.as_p())
        self.assertIn('style="#FF0C55"', form.as_p())
        self.assertIn('id="First_Name"', form.as_p())
        self.assertIn('name="First_Name"', form.as_p())
        self.assertIn('id="Last_Name"', form.as_p())
        self.assertIn('name="Last_Name"', form.as_p())
    
    def test_views_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/Events/AddEvent', {'Event_Name': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/Events/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)


 
 

 
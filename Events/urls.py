from django.urls import path
#from . import views
from Events.views import Events,AddEvent,AddMember

app_name = 'story6'

urlpatterns=[
    path('',Events,name="Events"),
    path('AddEvent',AddEvent,name='AddEvent'),
    path('AddMember/<event_id>',AddMember,name='AddMember')
]
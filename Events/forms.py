from django.forms import ModelForm
from django import forms
from .models import FormEvent,FormMember

class EventForm(forms.ModelForm):
    class Meta:
        model = FormEvent
        fields = [
            'Event_Name',
        ]

        widgets = {
            'Event_Name' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Event Name',
                    'style':'#FF0C55',
                    'id':'Event_Name',
                    'name':'Event_Name',
                    
                }
            ),
          
        }

class MemberForm(forms.ModelForm):
    class Meta:
        model = FormMember
        fields = [
            'First_Name',
            'Last_Name'
        ]

        widgets = {
            'First_Name' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Your First Name',
                    'style':'#FF0C55',
                    'id':'First_Name',
                    'name':'First_Name',
                    
                }
            ),
            'Last_Name' : forms.TextInput(
                attrs={
                    'class':'form-control',
                    'placeholder':'Enter Your Last Name',
                    'style':'#FF0C55',
                    'id':'Last_Name',
                    'name':'Last_Name',
                    
                }
            ),
          
        }
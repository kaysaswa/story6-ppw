from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from .forms import EventForm,MemberForm
from .models import FormMember,FormEvent

# Create your views here.

def Events(request):
    form = FormEvent.objects.all()
    member  = FormMember.objects.all()
    response={
        'form':form,
        'member':member
    }

    return render(request,'Events/Events.html',response)

def AddEvent(request):
    model = FormEvent
    form = EventForm()

    if request.method=="POST":
        form = EventForm(request.POST)
        if form.is_valid():
            Event_Name = request.POST.get("Event_Name")
        form.save()

        context = {
            'form' : form
        }

        return redirect('Events:Events')
        
    else:
        context = {
            'form' : form
        }
    
    return render(request,'Events/AddEvent.html',context) 
    
    #return render(request,'Events/AddEvent.html',response) 

def AddMember(request,event_id):
    model=FormMember
    form = MemberForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            Member=FormMember.objects.filter(event=event_id)
            event=FormEvent.objects.get(id=event_id)
            form.save()
            return redirect('Events:Events')

    context = {
        'form' : form,
        }

    return render(request,'Events/AddMember.html',context)  
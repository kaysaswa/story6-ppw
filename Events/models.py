
from django.db import models

# Create your models here.
class FormEvent(models.Model):
    Event_Name = models.CharField(max_length=20, )

    def __str__(self):
        return self.Event_Name

class FormMember(models.Model):
    First_Name = models.CharField(max_length=20)
    Last_Name = models.CharField(max_length=20)
    event = models.ForeignKey(FormEvent, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.First_Name


from django.urls import path,include

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('educations', views.educations, name='educations'),
    path('experiences', views.experiences, name='experiences'),
    path('project', views.project, name='project'),
    path('story1', views.story1, name='story1'),
    path('contact', views.contact, name='contact'),
    path('story5/', include('story5.urls',namespace='story5')),
    path('Events/', include('Events.urls',namespace='Events')),
]
